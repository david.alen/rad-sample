# Serverless to Terraform

Team: RAD

## Description

Serverless to Terraform is a tool to transform serverless files into terraform files.

## To deploy

### From serverless perspective

```bash
sls deploy --aws-profile=hackathon
```

### From terraform perspective

```bash
terraform apply
```
