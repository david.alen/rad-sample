# should build an api gateway for aws
module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = var.gateway_name
  description   = "RAD's awesome HTTP API Gateway"
  protocol_type = "HTTP"

  create_api_domain_name           = false  # to control creation of API Gateway Domain Name

  cors_configuration = {
    allow_headers = ["content-type", "x-amz-date", "authorization", "x-api-key", "x-amz-security-token", "x-amz-user-agent"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }

  tags = {
    Name = "http-apigateway"
  }
}