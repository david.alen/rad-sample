# should call gateway-api module and build an api gateway
provider "aws" {
  region = "us-east-1"
  profile = "hackathon"
}

module "rad-apigateway" {
  source = "./gateway"

  gateway_name = "rad-gateway"
}