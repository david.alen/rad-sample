async function handler() {
    const body = JSON.stringify({
        message: "fn-2 hello"
    })

    return {
        statusCode: 200,
        body
    }
}

module.exports = {
    handler
}