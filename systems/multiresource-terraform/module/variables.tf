variable "function_name" {
  type = string
  default = "function_name"
}

variable "handler" {
  type = string
  default = "index.handler"
}

variable "file_name" {
  type = string
}