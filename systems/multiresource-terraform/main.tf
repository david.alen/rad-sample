provider "aws" {
  profile = "hackathon"
  region = "us-east-1"
}

module "create_lambda" {
  source = "./module"
  handler = "fn-1/index.handler"

  for_each = toset(var.functions)
  function_name = each.key
  file_name = "../simple-functions/.serverless/example-serverless-api.zip"
}